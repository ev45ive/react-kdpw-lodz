import { connect } from "react-redux";
import { PlaylistForm } from "../components/PlaylistForm";
import { selectedPlaylist, playlistsUpdate } from "../reducers/playlists.reducer";
import { Playlist } from "../models/Playlist";
import { AppState } from "../store";
import React from "react";


export const SelectedPlaylistForm = connect(
    (state: AppState) => ({
        playlist: selectedPlaylist(state)!
    }),
    dispatch => ({
        onSave(draft: Playlist) {
            dispatch(playlistsUpdate(draft))
        }
    }),
    /* merge State,Dispatch and Own props */
    (stateProps, dispatchProps, ownProps) => ({
        ...stateProps,
        ...dispatchProps,
        ...ownProps
    })
)((props: any) => props.playlist && <PlaylistForm {...props} />)
export interface Entity {
    id: number
    name: string
}

export interface Playlist extends Entity {
    favorite: boolean
    /**
     * HEX Color Value
     */
    color: string
    tracks?: Track[]
}

export interface Track extends Entity { }

// const p: Playlist = {
//     id: 123,
//     name: '123'
// }

// switch (p.type) {
//     case 'Playlist':
//         p.type
//         break;
//     default:
//         p.type == "plaki"
// }

// if (p.tracks) {
//     p.tracks.forEach(() => { })
// }

// p.id
// if ('string' == typeof p.id) {
//     p.id
// }



// interface Point {
//     x: number, y: number
// }

// interface Vector {
//     x: number, y: number, length:number
// }

// let v: Vector = { x: 1, y: 2, length: 12 }
// let p: Point = { x: 1, y: 2 }
// p = v;
// v = p;
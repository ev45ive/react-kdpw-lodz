import { FC } from "react"
import React from "react"
import { AlbumCard } from "./AlbumCard"
import { Album } from "../models/Album"

type P = {
    results: Album[]
}

export const SearchResults: FC<P> = ({ results }) => {
    return <div className="card-group card-4-cols">
        {results.map(result =>
            <AlbumCard album={result}
                key={result.id}>
            </AlbumCard>
        )}
    </div>
}
import { Playlist } from "../models/Playlist";
import React from "react";

type S = {
  playlist: Playlist
}

type P = {
  playlist: Playlist
  onCancel(): void
  onSave(draft:Playlist): void
}

export class PlaylistForm extends React.PureComponent<P, S> {

  state: S = {
    playlist: {
      id: -1,
      name: '',
      color: '',
      favorite: false
    }
  }

  handleInput = (event: React.ChangeEvent<HTMLInputElement>) => {
    const target = event.target
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const fieldName = target.name

    // Dont change parent references! Wont rerender parent!
    // ;(this.state.playlist as any)[fieldName] = value
    // this.setState({})

    this.setState(prevState => ({
      playlist: prevState.playlist && {
        ...prevState.playlist,
        [fieldName]: value
      }
    }))
  }

  constructor(props: P) {
    super(props)
    console.log('constructor')
    // this.state.playlist = props.playlist! // antipattern, use getDerivedStateFromProp
    // this.setState({}) // cannot setState before mount!
  }

  componentDidMount() {
    console.log('componentDidMount')
  }

  componentWillUnmount() {
    console.log('componentWillUnmount')
  }

  static getDerivedStateFromProps(nextProps: P, nextState: S) {
    console.log('getDerivedStateFromProps', nextProps, nextState)

    return {
      playlist: nextState.playlist.id !== nextProps.playlist.id ?
        nextProps.playlist : nextState.playlist
    }
  }

  // shouldComponentUpdate(nextProps: P, nextState: S) {
  //   return nextProps.playlist !== this.props.playlist || nextState.playlist !== this.state.playlist
  // }

  render() {
    console.log('render')
    if (!this.state.playlist) {
      return <div>Please select playlist</div>
    }

    return (
      <div>
        <div className="form-group">
          <label>Name</label>
          <input type="text" className="form-control"
            value={
              this.state.playlist!.name
            } onChange={this.handleInput} name="name" />
          {170 - this.state.playlist.name.length} / 170
        </div>

        <div className="form-group">
          <label>
            <input type="checkbox" checked={this.state.playlist.favorite}
              onChange={this.handleInput} name="favorite" />
            Favorite
          </label>
        </div>

        <div className="form-group">
          <label>Color</label>
          <input type="color" value={this.state.playlist.color}
            onChange={this.handleInput} name="color" />
        </div>

        <input type="button" value="Cancel"
          onClick={this.props.onCancel} />
        <input type="button" value="Save" 
              onClick={() => this.props.onSave(this.state.playlist)} />
      </div>
    )
  }
}
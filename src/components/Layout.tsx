import React from "react"

export const Layout: React.FC = (props) => {

    return <div className="row">
        <div className="col">
            {props.children}
        </div>
    </div>
}



// export const Layout: React.FC<{
//     Placki: React.ComponentType<{ dane: any }>
// }> = ({ Placki }) => {

//     const dane = 123

//     return <div className="row">
//         <div className="col">
//             <Placki dane="123" />
//         </div>
//     </div>
// }
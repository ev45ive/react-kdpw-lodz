import React, { useState, useEffect, FC } from "react"
import { SelectedPlaylist } from "../containers/SelectedPlaylist"
import { PlaylistsList } from "../containers/PlaylistsList"
import { SelectedPlaylistForm } from "../containers/SelectedPlaylistForm"
import { RouteComponentProps } from "react-router"
import { useDispatch } from "react-redux"
import { playlistsSelect } from "../reducers/playlists.reducer"

export const PlaylistsViewRedux: FC<RouteComponentProps<{ playlist_id: string }>> = ({ match }) => {

    const [mode, setMode] = useState<'show' | 'edit'>('show')
    const dispatch = useDispatch()

    if (match.params.playlist_id) {
        dispatch(playlistsSelect(parseInt(match.params.playlist_id)))
    }

    return <div className="row">

        <div className="col">
            <PlaylistsList />
        </div>

        <div className="col">
            {mode == 'show' && <SelectedPlaylist onEdit={() => { setMode('edit') }} />}

            {mode == 'edit' && <SelectedPlaylistForm onCancel={() => { setMode('show') }} />}
        </div>

    </div>
}
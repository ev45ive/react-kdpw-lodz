import React from "react";
import { SearchField } from "../components/SearchField";
import { SearchResults } from "../components/SearchResults";
import { Album } from "../models/Album";
import { searchMusic } from "../services";
import { Route, RouteComponentProps } from "react-router";

type S = {
  results: Album[],
  message: string
}
type P = {} & RouteComponentProps<{}>

export class SearchView extends React.Component<P, S>{

  state: S = {
    results: [
      {
        id: '3qg53gt',
        name: 'Test',
        images: [
          { url: 'https://www.placecage.com/gif/300/300' }
        ]
      }
    ],
    message: ''
  }

  search = (query: string) => {

    this.props.history.push({
      search: '?query=' + query
    })

    searchMusic
      .searchAlbums(query)
      .then(
        results => this.setState({
          results
        }),
        err => this.setState({
          message: err.message
        })
      )
  }

  componentDidUpdate(prevProps:P) {
    const p = new URLSearchParams(this.props.location.search)
    const query = p.get('query')
    if (query && this.props.location.search !== prevProps.location.search) {
      this.search(query)
    }
  }

  render() {
    return (<>
      <div className="row">
        <div className="col">
          <SearchField onSearch={this.search}
            placeholder="Search albums" />
        </div>
      </div>
      <div className="row">
        <div className="col">
          {this.state.message}
          <SearchResults results={this.state.results} />
        </div>
      </div>
    </>)
  }
}
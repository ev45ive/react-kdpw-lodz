import React from 'react';
import { Layout } from './components/Layout';
import { Route, Switch, Redirect, NavLink } from 'react-router-dom';
import { PlaylistsViewRedux } from './views/PlaylistsViewRedux';
import { SearchView } from './views/SearchView';

class App extends React.Component {
  render() {
    return (
      <div className="App">
        <nav className="navbar navbar-expand navbar-dark bg-dark mb-3">
          <div className="container">

            <a className="navbar-brand" href="#">MusicApp</a>

            <div className="collapse navbar-collapse">
              <ul className="navbar-nav">

                <li className="nav-item">
                  <NavLink className="nav-link" activeClassName="placki active" to="/search">Search</NavLink>
                </li>

                <li className="nav-item">
                  <NavLink className="nav-link" to="/playlists">Playlists</NavLink>
                </li>

              </ul>
            </div>
          </div>
        </nav>

        <div className="container">
          <Layout>

            <Switch>
              <Redirect path="/" exact={true} to="/playlists" />

              <Route path="/playlists/:playlist_id" component={PlaylistsViewRedux} />
              <Route path="/playlists" component={PlaylistsViewRedux} />
              <Route path="/search" component={SearchView} />

              <Route path="*" render={() => <p>404 not found!</p>} />

            </Switch>

          </Layout>

        </div>
      </div>
    )
  }
}


export default App;

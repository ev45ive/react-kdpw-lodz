import React from 'react';
import { Album } from './models/Album';


export const searchCtx = React.createContext({
    query: '',
    results: [] as Album[],
    // es-lint-disable-next line @typescript-eslint/no-unused-vars
    search(query: string): Promise<Album[]> {
        throw Error('No Context provider');
    }
})